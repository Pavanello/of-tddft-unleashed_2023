# Orbital-Free TDDFT Unleashed


This is my talk at the 2023 TDDFT School and Workshop that took place in Newark, NJ, on 7/7/2023.

To execute the code at the end of the slides, you need to install dftpy simply typing

`pip install dftpy`

